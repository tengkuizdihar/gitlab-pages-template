---
layout: default
title: About Me
---
<!-- 
<img class="profile-picture" src="{{site.baseurl}}/{{site.profile-picture}}"> -->

How's it going, my name is Tengku Izdihar and welcome to my blog. I'm a programmer currently studying at a certain college in Indonesia. You can contact me by email, matrix, gitlab, and github. Information about those can be accessed by pressing "contact" up above in the top bar.
