---
draft : false
title : "Contact"
slug : "contact"
layout: default
---

Below are my contacts. I have no other public contact point besides listed below. Thank you for your understanding.

* Email     : tengkuizdihar@gmail.com
* Matrix    : tengkuizdihar:matrix.org
* Gitlab    : [tengkuizdihar](https://gitlab.com/tengkuizdihar)
* Github    : [tengkuizdihar](https://github.com/tengkuizdihar)

---