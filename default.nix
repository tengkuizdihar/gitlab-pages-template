with import <nixpkgs> {};

stdenv.mkDerivation {
    name = "gitlab-pages-jekyll";

    buildInputs = [
      jekyll
    ];
}
